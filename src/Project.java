import java.io.*;
import java.util.Base64;
import java.util.Comparator;

/**
 * This class represents a Project data object.
 * It includes:
 * projectExternalId - the ID of the project used within the company,
 * projectInternalID - the ID of the project used within the department,
 * projectName - name of the project,
 * projectIsFinished - status of the project,
 * projectCategories - list of categories project belongs to.
 *
 * @author Daria Tolmacheva
 */

public class Project implements Serializable {
    public final long projectExternalID;
    public final int projectInternalID;
    public final String projectName;
    public final boolean projectIsFinished;
    public final byte[] projectCategories;

    /**
     * Constructor method to create a project.
     * @param externalID
     * @param name
     * @param internalID
     * @param isFinished
     * @param categories
     */
    public Project(long externalID, String name, int internalID, boolean isFinished, byte[] categories){
        this.projectExternalID = externalID;
        this.projectName = name;
        this.projectInternalID = internalID;
        this.projectIsFinished = isFinished;
        this.projectCategories = categories;
    }

    /**
     * A method to serialize a Project object in a String form.
     * @return A string representing serialized object
     */
    public String serialize() {
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(this);
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (final IOException e) {
            // Print the exception stack trace
            // Might be better to send this to a log file?
            // e.printStackTrace();
            return "";
        }
    }

    /**
     * A method to deserialize a Project object from the String form.
     * @param serializedObject  Project object serialised into String form
     * @return A Project object represented by the String parameter.
     */
    public static Project deserialize(String serializedObject) {
        final byte[] data = Base64.getDecoder().decode(serializedObject);
        try (final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data))) {
            return (Project) ois.readObject();
        } catch (final IOException | ClassNotFoundException e) {
            // Print the exception stack trace
            // Might be better to send this to a log file?
            // e.printStackTrace();
            return null;
        }
    }

    /**
     * Comparator to sort projects by projectInternalID in ascending order.
     */
    public static Comparator<Project> ProjectInternalIDComparator
            = new Comparator<Project>() {

        public int compare(Project project1, Project project2) {

            int projectID_1 = project1.projectInternalID;
            int projectID_2 = project2.projectInternalID;

            return projectID_1 - projectID_2;
        }
    };

    /**
     * Comparator to sort projects by projectName in alphabetical order case-blindly.
     */
    public static Comparator<Project> ProjectNameComparator
            = new Comparator<Project>() {

        public int compare(Project project1, Project project2) {

            String projectName1 = project1.projectName.toUpperCase();
            String projectName2 = project2.projectName.toUpperCase();

            return projectName1.compareTo(projectName2);
        }
    };
}
