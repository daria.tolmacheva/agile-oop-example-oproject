import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class represents a repository of Project data objects.
 *
 * @author Daria Tolmacheva
 */

public class ProjectList {
    private ArrayList<Project> projectArrayList;

    /**
     * Constructor method to create a project list.
     */
    public ProjectList() {
        this.projectArrayList = new ArrayList<Project>();
    }

    /**
     * A method to find the size of ProjectList.
     * @return The number of Projects in the ProjectList.
     */
    public int size(){
        return projectArrayList.size();
    }

    /**
     * A method to add a Project to the ProjectList.
     * @param newProject A project to add to the list.
     */
    public void add(Project newProject) {
        projectArrayList.add(newProject);
    }

    /**
     * A method to get a Project at a given index of ProjectList.
     * @param index Index of the Project to be returned.
     * @return A Project stored at the given index in the ProjectList.
     */
    public Project get(int index){
        return projectArrayList.get(index);
    }

    /**
     * A method to delete a Project at a given index of ProjectList.
     * @param index Index of the Project to be deleted.
     */
    public void remove(int index){
        projectArrayList.remove(index);
    }

    /**
     * A method to find the index of a given Project in the ProjectList.
     * Returns -1 if project not found.
     * @param projectToFind A Project we want to know the index of.
     * @return An index og the given project.
     */
    public int indexOf(Project projectToFind){
        return projectArrayList.indexOf(projectToFind);
    }

    /**
     * A method to check if the ProjectList is empty.
     * @return A boolean showing if the ProjectList is empty.
     */
    public boolean isEmpty(){
        return projectArrayList.isEmpty();
    }

    /**
     * A method to check if a given Project is in the ProjectList.
     * @param projectToCheck A Project we want to check.
     * @return A boolean showing if the Project is in the ProjectList.
     */
    public boolean contains(Project projectToCheck){
        return projectArrayList.contains(projectToCheck);
    }

    /**
     * A method to get an iterator over the the ProjectList.
     * @return An iterator over the ProjectList starting in the beginning of ot.
     */
    public Iterator<Project> iterator(){
        return projectArrayList.iterator();
    }

    /**
     * A method to sort the ProjectList by the projectInternalID value in ascending order.
     */
    public void sortByInt(){
        projectArrayList.sort(Project.ProjectInternalIDComparator);
    }

    /**
     * A method to sort the ProjectList by the projectName value in alphabetic order case-blindly.
     */
    public void sortByName(){
        projectArrayList.sort(Project.ProjectNameComparator);
    }
}
