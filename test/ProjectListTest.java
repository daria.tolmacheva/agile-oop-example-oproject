import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

public class ProjectListTest {
    //Some projects to add to the lists when testing.
    Project testProject_1;
    Project testProject_2;
    Project testProject_3;

    @Before
    public void initializeProjects(){
        long externalID = 1234567890L;
        String name = "A Very Cool New Project";
        int internalID = 12345;
        boolean isFinished = false;
        byte categories[] = new byte[] {24, 35, 120};

        testProject_1 = new Project(externalID, name, internalID, isFinished, categories);

        long externalID_2 = 9876543210L;
        String name_2 = "A Boring Old Project";
        int internalID_2 = 54321;
        boolean isFinished_2 = true;
        byte categories_2[] = new byte[] {13, 36, 97, 125};

        testProject_2 = new Project(externalID_2, name_2, internalID_2, isFinished_2, categories_2);

        long externalID_3 = 5432167890L;
        String name_3 = "A Cool Old Project";
        int internalID_3 = 35124;
        boolean isFinished_3 = true;
        byte categories_3[] = new byte[] {13, 35};

        testProject_3 = new Project(externalID_3, name_3, internalID_3, isFinished_3, categories_3);
    }

    @Test
    public void createAProjectListAddTwoProjects() {
        ProjectList listOfProjects = new ProjectList();

        assertEquals(0, listOfProjects.size());

        listOfProjects.add(testProject_1);
        assertEquals(1, listOfProjects.size());
        listOfProjects.add(testProject_2);
        assertEquals(2, listOfProjects.size());

        assertEquals(testProject_1, listOfProjects.get(0));
        assertEquals(testProject_2, listOfProjects.get(1));
    }

    @Test
    public void deleteProjectsFromProjectList(){
        ProjectList listOfProjects = new ProjectList();

        listOfProjects.add(testProject_1);
        listOfProjects.add(testProject_2);

        listOfProjects.remove(0);
        assertEquals(1, listOfProjects.size());
        listOfProjects.remove(0);
        assertEquals(0, listOfProjects.size());
    }

    @Test
    public void findIndexOfAProjectInProjectList(){
        ProjectList listOfProjects = new ProjectList();

        listOfProjects.add(testProject_1);

        int foundIndex = listOfProjects.indexOf(testProject_1);
        assertEquals(0, foundIndex);
        foundIndex = listOfProjects.indexOf(testProject_2);
        assertEquals(-1, foundIndex);
    }

    @Test
    public void checkIfProjectListIsEmpty(){

        ProjectList listOfProjects = new ProjectList();
        assertTrue(listOfProjects.isEmpty());

        listOfProjects.add(testProject_1);
        assertFalse(listOfProjects.isEmpty());
    }

    @Test
    public void checkIfProjectListContainsTheProject(){

        ProjectList listOfProjects = new ProjectList();
        assertFalse(listOfProjects.contains(testProject_1));

        listOfProjects.add(testProject_1);
        assertTrue(listOfProjects.contains(testProject_1));
    }

    @Test
    public void iterateThroughTheProjectList(){
        ProjectList listOfProjects = new ProjectList();

        listOfProjects.add(testProject_1);
        listOfProjects.add(testProject_2);

        Iterator<Project> projectsIterator= listOfProjects.iterator();

        assertEquals(listOfProjects.get(0), projectsIterator.next());
        assertEquals(listOfProjects.get(1), projectsIterator.next());
    }

    @Test
    public void sortProjectListByInternalID(){
        ProjectList listOfProjects = new ProjectList();

        listOfProjects.add(testProject_2);
        listOfProjects.add(testProject_1);
        listOfProjects.add(testProject_3);

        listOfProjects.sortByInt();

        assertEquals(testProject_1, listOfProjects.get(0));
        assertEquals(testProject_3, listOfProjects.get(1));
        assertEquals(testProject_2, listOfProjects.get(2));
    }

    @Test
    public void sortProjectListByName(){
        ProjectList listOfProjects = new ProjectList();

        listOfProjects.add(testProject_1);
        listOfProjects.add(testProject_2);
        listOfProjects.add(testProject_3);

        listOfProjects.sortByName();

        assertEquals(testProject_2, listOfProjects.get(0));
        assertEquals(testProject_3, listOfProjects.get(1));
        assertEquals(testProject_1, listOfProjects.get(2));
    }

}
