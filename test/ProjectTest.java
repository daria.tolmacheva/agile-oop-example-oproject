import org.junit.Test;

import java.io.EOFException;
import java.io.IOException;
import java.util.Comparator;

import static org.junit.Assert.*;

public class ProjectTest {
    @Test
    public void createAProject() {
        long externalID = 1234567890L;
        String name = "A Very Cool New Project";
        int internalID = 12345;
        boolean isFinished = false;
        byte categories[] = new byte[] {24, 35, 120};

        Project testProject = new Project(externalID, name, internalID, isFinished, categories);

        assertEquals(externalID, testProject.projectExternalID);
        assertEquals(name, testProject.projectName);
        assertEquals(internalID, testProject.projectInternalID);
        assertEquals(isFinished, testProject.projectIsFinished);
        assertEquals(categories, testProject.projectCategories);
    }

    @Test
    public void serializeSameProjectConsistently(){
        long externalID = 1234567890L;
        String name = "A Very Cool New Project";
        int internalID = 12345;
        boolean isFinished = false;
        byte categories[] = new byte[] {24, 35, 120};

        Project testProject = new Project(externalID, name, internalID, isFinished, categories);

        String serialisedValue1 = testProject.serialize();
        String serialisedValue2 = testProject.serialize();

        assertFalse(serialisedValue1.isEmpty());
        assertEquals(serialisedValue1, serialisedValue2);
    }

    @Test
    public void serializeDifferentProjectsConsistently(){
        long externalID = 1234567890L;
        String name = "A Very Cool New Project";
        int internalID = 12345;
        boolean isFinished = false;
        byte categories[] = new byte[] {24, 35, 120};

        Project testProject_1 = new Project(externalID, name, internalID, isFinished, categories);
        String serialisedValue1 = testProject_1.serialize();

        long externalID_2 = 9876543210L;
        String name_2 = "A Boring Old Project";
        int internalID_2 = 54321;
        boolean isFinished_2 = true;
        byte categories_2[] = new byte[] {13, 36, 97, 125};

        Project testProject_2 = new Project(externalID_2, name_2, internalID_2, isFinished_2, categories_2);
        String serialisedValue2 = testProject_2.serialize();

        assertNotEquals(serialisedValue1, serialisedValue2);
    }

    @Test
    public void deserializeProjectCorrectly(){
        long externalID = 1234567890L;
        String name = "A Very Cool New Project";
        int internalID = 12345;
        boolean isFinished = false;
        byte categories[] = new byte[] {24, 35, 120};

        Project serializedProject = new Project(externalID, name, internalID, isFinished, categories);

        String serialisedValue = serializedProject.serialize();
        Project deserializedProject = Project.deserialize(serialisedValue);

        assertNotEquals(null, deserializedProject);
        assertEquals(serializedProject.projectExternalID, deserializedProject.projectExternalID);
        assertEquals(serializedProject.projectName, deserializedProject.projectName);
        assertEquals(serializedProject.projectInternalID, deserializedProject.projectInternalID);
        assertEquals(serializedProject.projectIsFinished, deserializedProject.projectIsFinished);
        assertArrayEquals(serializedProject.projectCategories, deserializedProject.projectCategories);
    }

    @Test
    public void deserializeWrongProject() {
        String wrongSerialisedValue = "";
        Project deserializedProject = Project.deserialize(wrongSerialisedValue);

        assertNull(deserializedProject);
    }
}
